﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGUI
{
    class User
    {
        public string username { get; set; }

        public User(string username)
        {
            this.username = username;
        }

        public override string ToString() 
        {
            return username;
        }
    }
}
