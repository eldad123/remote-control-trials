﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGUI
{
    class ClientAddresses
    {
        public string PublicAddress { get; set; }
        public string PrivateAddress { get; set; }


        public ClientAddresses()
        {
            PublicAddress = "";
            PrivateAddress = "";
        }

        public ClientAddresses(string pubAddr, string privAddr)
        {
            PublicAddress = pubAddr;
            PrivateAddress = privAddr;
        }

        public static string GetIp(string address)
        {
            return address.Substring(0, address.IndexOf(':'));
        }

        public static int GetPort(string address)
        {
            return Int32.Parse(address.Substring(address.IndexOf(':') + 1));
        }
    }
}
