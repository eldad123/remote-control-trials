﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ClientGUI
{
    class Control
    {
        [DllImport(@"ControLib\Debug\control.dll")]
        public static extern int controlMain(short control,
            string pub_ip, int pub_port,
            string priv_ip, int priv_port,
            int bind_port);
    }
}
