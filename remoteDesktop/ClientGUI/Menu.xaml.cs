﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        private string myUsername;
        private string otherSideUsername;
        private string selectedRow = null;
        private Socket server_sock;
        internal string SelectedRow { get => selectedRow; set => selectedRow = value; }

        public Menu(string username, Socket sock)
        {
            this.server_sock = sock;
            this.myUsername = username;
            InitializeComponent();


            byte[] buffer = new byte[256];
            int bytesRec = sock.Receive(buffer);
            string response = Encoding.ASCII.GetString(buffer, 0, bytesRec);
            List<string> users = JsonConvert.DeserializeObject<List<string>>(response);
            dataGrid.ItemsSource = users;
        }


        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            if (selectedRow != null)
            {
                connectToPeer(selectedRow, 1);
            }
            else
            {
                connectToPeer("wait", 0);
            }
        }

        private void connectToPeer(string username, short control)
        {
            byte[] msg = Encoding.ASCII.GetBytes(username);
            // Send the data through the socket.
            int bytesSent = server_sock.Send(msg);

            byte[] buffer = new byte[256];
            // Receive the response from the remote device.  
            int bytesRec = server_sock.Receive(buffer);

            //string response = Encoding.ASCII.GetString(buffer, 0, bytesRec);
            ClientAddresses peerAddresses = new ClientAddresses();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(buffer));

            peerAddresses.PrivateAddress = json.Value<string>("PrivateAddress");
            peerAddresses.PublicAddress = json.Value<string>("PublicAddress");

            int MyPrivPort = ClientAddresses.GetPort(server_sock.LocalEndPoint.ToString());

            int remotePrivPort = ClientAddresses.GetPort(peerAddresses.PublicAddress);
            string remotePrivIP = ClientAddresses.GetIp(peerAddresses.PublicAddress);

            int remotePubPort = ClientAddresses.GetPort(peerAddresses.PublicAddress);
            string remotePubIP = ClientAddresses.GetIp(peerAddresses.PublicAddress);

            int answer = Control.controlMain(control, remotePubIP, remotePubPort, remotePrivIP, remotePrivPort, MyPrivPort);

            int x = answer;
        }

        //refresh the list of rooms
        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            //TODO:: ask for refresh
        }

       
        //function will get the room that was selected
        private void ItemsView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0)
            {
                string username = (string)(e.AddedItems[0]);
                this.selectedRow = username;
                this.otherSideUsername = username;
            }
            else
            {
                this.selectedRow = null;
            }
        }

        private void dataGrid_OnUnSelected(object sender, MouseButtonEventArgs e)
        {
            ((DataGrid)sender).SelectedItem = null;
            this.selectedRow = null;
        }
    }
}
