﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for SignIn.xaml
    /// </summary>
    public partial class SignIn : Page
    {
        public SignIn()
        {
            InitializeComponent();
        }
        //This function detect when the HyperLink is pressed and navigate to signun page when he does
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            //navigate no signin page
            this.NavigationService.Navigate(new SignUp());
        }

        //This fuction is activate when the enter key is pressed
        private void OnEnterKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SignInAction();
            }
        }

        //this function check when the signin button is pressed 
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SignInAction();
        }

        private void SignInAction()
        {
            if(username.Text != string.Empty)
            {
                //IPHostEntry ipHostInfo = Dns.GetHostEntry("139.162.171.230");
                IPAddress ipAddress = IPAddress.Parse("139.162.171.230");
                IPEndPoint serverEP = new IPEndPoint(ipAddress, 1337);

                // Create a TCP/IP  socket.  
                Socket server_sock = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                server_sock.Connect(serverEP);

                //get the private address(ip:port) connection of the socket
                string MyprivateAddress = server_sock.LocalEndPoint.ToString();

                // Encode the data string into a byte array.  
                byte[] msg = Encoding.ASCII.GetBytes(username.Text + "," + MyprivateAddress);

                // Send the data through the socket.  
                int bytesSent = server_sock.Send(msg);

                this.NavigationService.Navigate(new Menu(username.Text, server_sock));
            }
        }
    }
}
