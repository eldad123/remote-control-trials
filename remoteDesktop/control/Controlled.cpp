#include "Controlled.h"

#include <gdiplus.h>
#include <atlimage.h>

#pragma comment(lib,"gdiplus.lib")

using namespace Gdiplus;

Controlled::Controlled() : _hScreenDC(), _imageBmp(), _imageBmpOld()
{
    HWND DesktopHwnd = GetDesktopWindow();
    // get the device context of the screen
    _hScreenDC = GetDC(DesktopHwnd);
    // and a device context to put it in
    GetWindowRect(DesktopHwnd, &_DesktopParams);
    cx = _DesktopParams.right - _DesktopParams.left;
    cy = _DesktopParams.bottom - _DesktopParams.top;
    _hMemoryDC = CreateCompatibleDC(_hScreenDC);
    SetStretchBltMode(_hMemoryDC, HALFTONE);
}

Controlled::~Controlled()
{
    ReleaseDC(NULL, _hScreenDC);
    DeleteDC(_hScreenDC);

    // once the user quits....
    DeleteObject(_imageBmp);
    DeleteObject(_imageBmpOld);
    DeleteDC(_hMemoryDC);
}

std::vector<char> Controlled::screenShot()
{
    _imageBmp = CreateCompatibleBitmap(_hScreenDC, cx, cy);
    _imageBmpOld = (HBITMAP)SelectObject(_hMemoryDC, _imageBmp);

    BitBlt(_hMemoryDC, 0, 0, cx, cy, _hScreenDC, 0, 0, MERGECOPY); // draw the image to the screen DC

    CURSORINFO cursor = { sizeof(CURSORINFO) };
    GetCursorInfo(&cursor);
    if (cursor.flags == CURSOR_SHOWING)
    {
        ICONINFOEXW hIcon = { sizeof(ICONINFOEXW) };
        GetIconInfoExW(cursor.hCursor, &hIcon);
        int x = cursor.ptScreenPos.x - _DesktopParams.left - _DesktopParams.left - hIcon.xHotspot;
        int y = cursor.ptScreenPos.y - _DesktopParams.top - _DesktopParams.top - hIcon.yHotspot;
        BITMAP bmpCursor = { 0 };
        GetObject(hIcon.hbmColor, sizeof(BITMAP), &bmpCursor);
        DrawIconEx(_hMemoryDC, x, y, cursor.hCursor, bmpCursor.bmWidth, bmpCursor.bmHeight, 0, 0, DI_NORMAL);
    }

    std::vector<char> buffer = bmpToJpg(_imageBmp);

    return buffer;
}


std::vector<char> Controlled::bmpToJpg(HBITMAP& hBitmap)
{
    std::vector<char> buffer;
    GdiplusStartupInput gdiplusStartupInput;
    ULONG_PTR gdiplusToken;
    GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    HGLOBAL hg = NULL;
    compressImgToStream(hBitmap, hg);

    // copy IStream to buffer
    int bufsize = GlobalSize(hg);
    unsigned char* charBuff = new unsigned char[bufsize];
    LPVOID pimage = GlobalLock(hg);
    buffer.resize(bufsize);
    memcpy(&buffer[0], pimage, bufsize);
    GlobalUnlock(hg);
    GdiplusShutdown(gdiplusToken);

    return buffer;
}

void Controlled::compressImgToStream(HBITMAP& hBitmap, HGLOBAL& hg)
{
    IStream* stream = NULL;
    CreateStreamOnHGlobal(0, FALSE, &stream);
    Bitmap* image = new Bitmap(hBitmap, nullptr);

    CLSID             encoderClsid;
    EncoderParameters encoderParameters;
    ULONG             quality;

    GetEncoderClsid(L"image/jpeg", &encoderClsid);

    encoderParameters.Count = 1;
    encoderParameters.Parameter[0].Guid = EncoderQuality;
    encoderParameters.Parameter[0].Type = EncoderParameterValueTypeLong;
    encoderParameters.Parameter[0].NumberOfValues = 1;

    // Save the image as a JPEG with quality level x.
    quality = 10;
    encoderParameters.Parameter[0].Value = &quality;
    image->Save(stream, &encoderClsid, &encoderParameters);
    GetHGlobalFromStream(stream, &hg);
    stream->Release();
    delete image;
}

INT Controlled::GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
    UINT  num = 0;          // number of image encoders
    UINT  size = 0;         // size of the image encoder array in bytes

    ImageCodecInfo* pImageCodecInfo = NULL;

    GetImageEncodersSize(&num, &size);
    if (size == 0)
        return -1;  // Failure

    pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
    if (pImageCodecInfo == NULL)
        return -1;  // Failure

    GetImageEncoders(num, size, pImageCodecInfo);

    for (UINT j = 0; j < num; ++j)
    {
        if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
        {
            *pClsid = pImageCodecInfo[j].Clsid;
            free(pImageCodecInfo);
            return j;  // Success
        }
    }

    free(pImageCodecInfo);
    return -1;  // Failure
}




void Controlled::press(Pressing& pressing)
{
    mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE | pressing.leftOrRightDown, pressing.xAbs, pressing.yAbs, NULL, NULL);
    mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE | pressing.leftOrRightUp, pressing.xAbs, pressing.yAbs, NULL, NULL);
}


void Controlled::demoKeyBoard(unsigned int long long vkCode)
{
    keybd_event(vkCode, 0,
        0, //press
        0);

    // Simulate a key release
    keybd_event(vkCode, 0,
        KEYEVENTF_KEYUP, //release
        0);
}


int Controlled::getCx()
{
    return this->cx;
}

int Controlled::getCy()
{
    return this->cy;
}
