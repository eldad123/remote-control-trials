#pragma once
#include "Structs.h"
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>


using namespace std;

class Controlled
{

private:

	HBITMAP     _imageBmp;       // the actual bitmap which contains the image (will be put in the DC)
	HBITMAP     _imageBmpOld;    // the DC's old bitmap (for cleanup)

	HDC _hScreenDC;
	RECT _DesktopParams;
	HDC _hMemoryDC;

	int cx;
	int cy;

public:

	Controlled();
	~Controlled();
	std::vector<char> screenShot();
	std::vector<char> bmpToJpg(HBITMAP& hBitmap);
	void compressImgToStream(HBITMAP& hBitmap, HGLOBAL &hg);
	static INT GetEncoderClsid(const WCHAR* format, CLSID* pClsid);


	void press(Pressing& pressing);
	void demoKeyBoard(unsigned int long long vkCode);
	int getCx();
	int getCy();
};