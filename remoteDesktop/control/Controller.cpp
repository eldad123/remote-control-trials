#include "Controller.h"
#include <atlimage.h>

#define SHOW 5

std::mutex _mtx;

std::queue<Pressing> _mousePressings;
std::queue<unsigned int long long> _keyboardCodes;

Controller::Controller() : buffersToUse(), _imageBmp(), _imageBmpOld(), _wnd()
{
    _imageDC = CreateCompatibleDC(NULL);     // create an offscreen DC
    HWND DesktopHwnd = GetDesktopWindow();
    RECT DesktopParams;
    GetWindowRect(DesktopHwnd, &DesktopParams);
    cx = DesktopParams.right - DesktopParams.left;
    cy = DesktopParams.bottom - DesktopParams.top;
}

Controller::~Controller()
{
    // once the user quits....
    DeleteObject(_imageBmp);
    DeleteObject(_imageBmpOld);
    DeleteDC(_imageDC); // delete the DC we created
}


// Function to load the image into the DC so we can draw it to the screen
void Controller::loadImage()
{
    if (buffersToUse.empty())
        return;

    std::vector<char> buffer = buffersToUse.front();
    std::unique_lock<std::mutex> buffers_lock(_mtx, std::defer_lock);

    buffers_lock.lock();
    buffersToUse.pop();
    buffers_lock.unlock();

    CImage image;
    HGLOBAL hMem = GlobalAlloc(GMEM_FIXED, buffer.size());
    LPVOID pmem = GlobalLock(hMem);
    memcpy(pmem, &buffer[0], buffer.size());
    IStream* stream = NULL;
    CreateStreamOnHGlobal(hMem, FALSE, &stream);
    image.Load(stream);
    stream->Release();
    GlobalUnlock(hMem);
    GlobalFree(hMem);
    _imageBmp = (HBITMAP)image;
    _imageBmpOld = (HBITMAP)SelectObject(_imageDC, _imageBmp);  // put the loaded image into the DC
}

void Controller::drawImage(HDC screen, HWND wnd)
{
    int width = 0;
    int height = 0;
    getCurrSizeWnd(width, height, wnd);

    SetStretchBltMode(screen, HALFTONE);
    StretchBlt(screen, 0, 0, width, height,
        _imageDC, 0, 0, cx, cy, SRCCOPY);
}

void Controller::handleWindow()
{
    // create the window
    this->_wnd = this->createWindow(NULL);

    MSG msg;

    ShowWindow(_wnd, SHOW);
    //UpdateWindow(_wnd);

    std::thread load_and_draw_t(&Controller::loadAndDraw, this);

    while (GetMessage(&msg, NULL, 0, 0) > 0) // while we are getting non-WM_QUIT messages...
    {
        TranslateMessage(&msg);     // translate them
        DispatchMessage(&msg);      // and dispatch them (the wndProc will process them)
    }

    load_and_draw_t.join();

    DestroyWindow(_wnd);
}

void Controller::loadAndDraw()
{
    HDC screen = GetDC(_wnd);

    while (true)
    {
        this->loadImage();  // load the image
        this->drawImage(screen, _wnd);   // draw the image to the screen DC
    }


    ReleaseDC(_wnd, screen);  // clean up
    DeleteDC(screen);
}



// A callback to handle Windows messages as they happen
LRESULT CALLBACK Controller::wndProc(HWND wnd, UINT msg, WPARAM vkCode, LPARAM l)
{
    wchar_t whatPressed[32];
    // what kind of message is this?
    bool pressed = false;
    switch (msg)
    {
    case WM_SYSCHAR:
        swprintf_s(whatPressed, L"WM_SYSCHAR: %c\n", (wchar_t)vkCode);
        OutputDebugString((LPCSTR)whatPressed);
        pressed = true;
        break;

    case WM_KEYDOWN:
        swprintf_s(whatPressed, L"WM_KEYDOWN: 0x%x\n", vkCode);
        OutputDebugString((LPCSTR)whatPressed);
        pressed = true;
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    }
    if (pressed)
    {
        //Controller::_keyboardCodes.push(vkCode);
    }
    // for everything else, the default window message handler do its thing
    return DefWindowProc(wnd, msg, vkCode, l);
}


void Controller::getPress()
{
    bool pressed = false;
    while (!pressed)
    {
        //get cursor position relative to the showing window
        POINT p;
        GetCursorPos(&p);
        ScreenToClient(_wnd, &p);
        //cout << p.x << ", " << p.y << endl;

        //get the showing window width and height
        int wndWidth = 0, wndHeight = 0;
        getCurrSizeWnd(wndWidth, wndHeight, _wnd);

        //find which side of mouse have pressed
        int leftOrRightDown = 0, leftOrRightUp = 0;
        if (p.x >= 0 && p.x <= wndWidth && p.y >= 0 && p.y <= wndHeight)
        {
            pressed = leftOrRightPressed(leftOrRightDown, leftOrRightUp);
        }

        //press the mouse
        if (pressed)
        {
            //cout << "cx: " << cx << ", cy:" << cy << endl;
            float xPress = p.x * (float)((float)cx / wndWidth);
            float yPress = p.y * (float)((float)cy / wndHeight);
            float xAbs = ((float)ABSOLUTE_SIZE / cx * xPress);
            float yAbs = ((float)ABSOLUTE_SIZE / cy * yPress);

            Pressing pressing
            {
                leftOrRightDown,
                leftOrRightUp,
                xAbs,
                yAbs
            };

            //Controller::_mousePressings.push(pressing);
        }
    }

}






void Controller::getCurrSizeWnd(int& width, int& height, HWND wnd)
{
    RECT rect;
    if (GetWindowRect(wnd, &rect))
    {
        width = rect.right - rect.left - SIDES_GAP;
        height = rect.bottom - rect.top - UP_AND_DOWN_GAP;
    }
}


bool Controller::leftOrRightPressed(int& leftOrRightDown, int& leftOrRightUp)
{
    if ((GetKeyState(VK_LBUTTON) & 0x8000) != 0)
    {
        leftOrRightDown = MOUSEEVENTF_LEFTDOWN;
        leftOrRightUp = MOUSEEVENTF_LEFTUP;
        return true;
    }
    else if ((GetKeyState(VK_RBUTTON) & 0x8000) != 0)
    {
        leftOrRightDown = MOUSEEVENTF_RIGHTDOWN;
        leftOrRightUp = MOUSEEVENTF_RIGHTUP;
        return true;
    }
    return false;
}


void Controller::pushBuffer(std::vector<char> buffer)
{
    this->buffersToUse.push(buffer);
}

queue<vector<char>>& Controller::getBuffers()
{
    return this->buffersToUse;
}

int Controller::getCx()
{
    return this->cx;
}

int Controller::getCy()
{
    return this->cy;
}



// A function to create the window and get it set up
HWND Controller::createWindow(HINSTANCE inst)
{
    WNDCLASSEX wc = { 0 };        // create a WNDCLASSEX struct 
    wc.cbSize = sizeof(WNDCLASSEX);     // tell windows the size of this struct
    wc.hCursor = LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW));  //  use the normal arrow cursor for this window
    wc.hInstance = inst;
    wc.lpfnWndProc = wndProc;                // use the wndProc function to handle messages
    wc.lpszClassName = TEXT("DisplayScreen");   // window class a name

    RegisterClassEx(&wc);           // register the window class with Windows

    // the style of the window 
    int style = WS_OVERLAPPEDWINDOW | WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_DLGFRAME | WS_VISIBLE;

    RECT rc = { 0,0,SCREEN_WIDTH,SCREEN_HEIGHT };      // desired rect
    AdjustWindowRect(&rc, style, FALSE);              // adjust the rect with the given style, FALSE because there is no menu

    return CreateWindow(
        TEXT("DisplayScreen"),       // the name of the window class to use for this window (the one just registered)
        TEXT("Remote Control"),   // the text to appear on the title of the window
        style,         // the style of this window 
        80, 80,                    // create it at position 80,80
        SCREEN_WIDTH,         // width of the window we want
        SCREEN_HEIGHT,         // height of the window
        NULL, NULL,                  // no parent window, no menu
        inst,                       // the program instance
        NULL);                      // no extra parameter
}


