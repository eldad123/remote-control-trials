#pragma once
#include "Structs.h"
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <queue>

using namespace std;

#define SCREEN_WIDTH 1100
#define SCREEN_HEIGHT 600
#define SIDES_GAP 19
#define UP_AND_DOWN_GAP 39
#define ABSOLUTE_SIZE 65536


class Controller
{
private:
	HDC         _imageDC;        // the DC to hold the image
	HBITMAP     _imageBmp;       // the actual bitmap which contains the image (will be put in the DC)
	HBITMAP     _imageBmpOld;    // the DC's old bitmap (for cleanup)

	HWND _wnd;

	int cx;
	int cy;

	std::queue<vector<char>> buffersToUse;

public:
	Controller();

	~Controller();

	static std::queue<Pressing> _mousePressings;
	static std::queue<unsigned int long long> _keyboardCodes;


	void loadImage();

	void drawImage(HDC screen, HWND wnd);

	HWND createWindow(HINSTANCE inst);

	void getCurrSizeWnd(int& width, int& height, HWND wnd);

	static LRESULT CALLBACK wndProc(HWND wnd, UINT msg, WPARAM w, LPARAM l);

	void handleWindow();

	void loadAndDraw();

	void pushBuffer(std::vector<char> buffer);

	void getPress();

	bool leftOrRightPressed(int& leftOrRightDown, int& leftOrRightUp);

	queue<vector<char>>& getBuffers();

	int getCx();
	int getCy();
};