#pragma once

typedef struct Pressing
{
	int leftOrRightDown;
	int leftOrRightUp;
	float xAbs;
	float yAbs;
};