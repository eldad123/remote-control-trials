#include "udpClient.h"
#include "Controlled.h"
#include "Controller.h"
#include "Structs.h"

#define CONTROLLER 1
#define CONTROLLED 0

std::queue<Pressing> Controller::_mousePressings;
std::queue<unsigned int long long> Controller::_keyboardCodes;

bool HolePunch(udpCLient& peer, int times);
void handleController(udpCLient& client);
void handleControlled(udpCLient& client);

extern "C" __declspec(dllexport) int controlMain(
	short control,
	char* pub_ip, int pub_port,
	char* priv_ip, int priv_port,
	int bind_port)
{
	try
	{
		udpCLient peer(pub_ip, pub_port, bind_port);

		if (HolePunch(peer, 10))
		{
			if (control == CONTROLLER)
			{
				handleController(peer);
			}
			else if (control == CONTROLLED)
			{
				handleControlled(peer);
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}
	catch (std::exception& e)
	{
	}
}


void handleControlled(udpCLient& peer)
{
	Controlled cd;
	int count = 0;
	while (true)
	{
		peer.send(cd.screenShot());
		count++;
		std::cout << count << std::endl;
	}
}

void handleController(udpCLient& peer)
{
	Controller cr;
	int size = 0;
	std::vector<char> buffer;
	std::thread handle_window_t(&Controller::handleWindow, &cr);

	while (true)
	{
		size = peer.available();
		if (size != 0)
		{
			buffer.resize(size);
			peer.receive(buffer);

			cr.pushBuffer(buffer);
		}

	}
	handle_window_t.join();
}


bool HolePunch(udpCLient& peer, int times)
{
	std::vector<char> recBuff(1);
	std::vector<char> sendBuff = { 'h' }; // aka hole punch

	int i = 0;
	while (i < times && !peer.available())
	{
		std::cout << "sending messege: " + std::to_string(i + 1) << std::endl;
		peer.send(sendBuff);
		++i;
		peer.wait(1);
	}

	if (peer.available())
	{
		do
		{
			peer.wait(1);
			recBuff.resize(peer.available());
			peer.receive(recBuff);
		} while (peer.available() && recBuff[0] == 'h');

		return true;
	}
	else
	{
		return false;
	}
}




