#include "udpClient.h"

udpCLient::udpCLient(std::string remote_address, int remote_port, int bind_port) :
	_error_code(), _io_context(), _socket(_io_context), _remote_endpoint(asio::ip::make_address_v4(remote_address, _error_code), remote_port),
	_bind_endpoint(asio::ip::address_v4::any(), bind_port)
{
	_socket.open(asio::ip::udp::v4(), _error_code);
	if (_error_code)
	{
		throw std::exception("error creating udpClient.");
	}

	_socket.bind(_bind_endpoint, _error_code);
	if (_error_code)
	{
		throw std::exception("error binding");
	}
}

udpCLient::~udpCLient()
{
	_socket.cancel();
}



size_t udpCLient::receive(std::vector<char>& message)
{
	//asio::read(_socket, asio::buffer(message, 5));
	return _socket.receive_from(asio::buffer(message), _remote_endpoint);
}


void udpCLient::send(std::vector<char> message)
{
	_socket.send_to(asio::buffer(message), _remote_endpoint, 0 , _error_code);

	if (_error_code)
	{
		throw std::exception(_error_code.message().c_str());
	}
}

void udpCLient::wait(int seconds)
{
	asio::steady_timer t(_io_context, asio::chrono::seconds(seconds));
	t.async_wait(&udpCLient::timerHandler);
	t.wait();
}


size_t udpCLient::available()
{
	return _socket.available();
}


void udpCLient::timerHandler(const asio::error_code& e)
{
	if (e)
	{
		throw std::exception("Asio timer failed with error: " + e.value());
	}
}


