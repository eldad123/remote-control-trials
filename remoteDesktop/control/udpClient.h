#pragma once
#include <asio.hpp>

class udpCLient
{
public:
	asio::io_context _io_context;

	udpCLient(std::string remote_address, int remote_port, int bind_port);
	~udpCLient();

	void send(std::vector<char> message);
	size_t receive(std::vector<char>& message);

	void wait(int seconds);
	size_t available();

private:
	asio::error_code _error_code;
	asio::ip::udp::socket _socket;
	asio::ip::udp::endpoint _remote_endpoint;
	asio::ip::udp::endpoint _bind_endpoint;

	static void timerHandler(const asio::error_code& e);
};